let nextScroll;

function scrollTo(dest, duration = 500) {
	const frequence = 10;
	const to = Math.min(dest.offsetTop, document.body.clientHeight - window.innerHeight);
	let timeLeft = duration;
	nextScroll = () => {
		if ((document.body.scrollTop || document.documentElement.scrollTop || 0) === to) {
			nextScroll = () => 0;
			return;
		}
		currPos = window.pageYOffset + (to - window.pageYOffset) / (timeLeft / frequence);
		timeLeft -= frequence;
		document.body.scrollTop = document.documentElement.scrollTop = currPos;
		setTimeout(nextScroll, frequence);
	};
	nextScroll();
}

function highlight(target) {
	target.classList.add("highlight");
	setTimeout(() => target.classList.remove("highlight"), 1000);
}

document.querySelectorAll('a[href^="#"]').forEach(
	link => link.addEventListener('click',
		e => {
			e.preventDefault();
			const target = document.getElementById(e.target.href.split('#')[1]);
			scrollTo(target);
			highlight(target);
		}
	)
);